import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {
  foto;
  marca: string;
  modelo: string;
  fecha: number;
  precio:number;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  guardarMoto(){
    this.foto = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    var form = new FormData();
    form.append("foto", this.foto);
    form.append("marca", this.marca);
    form.append("modelo", this.modelo);
    form.append("fecha", this.fecha + "");
    form.append("precio", this.precio + "");

    fetch("http://motos.puigverd.org:80/moto/foto", {
      "method": "POST",
      "body": form
    }).then(response => { this.router.navigate(["home"])})
    // Se añade y vuelve pero no refresca automáticamente
  }

}
