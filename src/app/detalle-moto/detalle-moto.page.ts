import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-moto',
  templateUrl: './detalle-moto.page.html',
  styleUrls: ['./detalle-moto.page.scss'],
})
export class DetalleMotoPage implements OnInit {
  data:any;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        // (no cal)
        console.log(this.data);
      }
    });
  }

  ngOnInit() {
  }

  async borrarMoto() {
    const alert = await this.alertController.create({
      header: 'Estás seguro de que quieres eliminar esta moto?',
      buttons: [
        {
          text: 'No borrar',
          role: 'cancel',
        }, {
          text: 'Borrar',
          handler: () => {
            fetch("http://motos.puigverd.org/moto/" + this.data.id, {
              "method": "DELETE",
              "mode": "cors"
            }).then(response => {this.router.navigate(["home"]);});
          }
        }
      ]
    });
    await alert.present();
  }

}
