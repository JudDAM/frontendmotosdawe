import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  motos:[];

  constructor(private router: Router, private menu: MenuController) {}
  
  ionViewWillEnter(){
    this.getTodasMotos();
  }

  async getTodasMotos(){
    const respuesta = await fetch("http://motos.puigverd.org/motos");
    this.motos = await respuesta.json();
    console.log(this.motos);
  }
  
  openMenu() {
    this.menu.enable(true, 'menu');
    this.menu.open('menu');
  }
  
  async filtrarPorMarca(marca){
    if(marca=="Ducati" || marca=="Yamaha" || marca=="Honda"){
      const respuesta = await fetch("http://motos.puigverd.org/motos?marca="+marca);
      this.motos= await respuesta.json();
    }else this.getTodasMotos();
    this.menu.close('menu');
  }

  detalleMoto(index){
    var navigationExtras: NavigationExtras = {
      state: {
        parametros: this.motos[index]
      }
    };
    this.router.navigate(['detalle-moto'], navigationExtras);
  }

  anadirMoto(){
    this.router.navigate(["add-moto"]);
  }
}
